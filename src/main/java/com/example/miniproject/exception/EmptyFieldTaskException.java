package com.example.miniproject.exception;

public class EmptyFieldTaskException extends RuntimeException{
    public EmptyFieldTaskException(String message) {
        super(message);
    }
}
