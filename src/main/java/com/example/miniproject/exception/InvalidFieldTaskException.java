package com.example.miniproject.exception;

public class InvalidFieldTaskException extends RuntimeException{
    public InvalidFieldTaskException(String message) {
        super(message);
    }
}
