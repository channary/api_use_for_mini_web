package com.example.miniproject.controller;
import com.example.miniproject.exception.InvalidFieldCategoryException;
import com.example.miniproject.model.entity.Category;
import com.example.miniproject.model.entity.UserInfo;
import com.example.miniproject.model.response.CategoryResponse;
import com.example.miniproject.model.request.CategoryRequest;
import com.example.miniproject.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@SecurityRequirement(name = "bearerAuth")
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public Integer getUserCurrentId(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = (UserInfo) authentication.getPrincipal();
        int userId = userInfo.getId();
        return userId;
    }

    @PostMapping("/categories")
    @Operation(summary = "Add New Category")
    public ResponseEntity<CategoryResponse<Category>> addNewCategory(@RequestBody CategoryRequest categoryRequest){
        Integer storeNewCategoryId = categoryService.addNewCategory(categoryRequest,getUserCurrentId());
        CategoryResponse<Category> response = CategoryResponse.<Category>builder()
              .payload(categoryService.getCategoryById(storeNewCategoryId))
                .date(new Timestamp(System.currentTimeMillis()))
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/categories/delete/{id}")
    @Operation(summary = "Delete category by id for current user")
    public ResponseEntity<CategoryResponse<String>> deleteCategoryById(@PathVariable("id") Integer categoryId){
        CategoryResponse<String> response = null;
        if (categoryService.deleteCategory(categoryId,getUserCurrentId()) == true) {
            response = CategoryResponse.<String>builder()
                    .payload("Delete this category id " + categoryId +" is successful !!")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .success(true)
                    .build();
        }
        return ResponseEntity.ok(response);
    }

    @GetMapping("/categories")
    @Operation(summary = "Get all Category")
    public List<Category> getAllCategory(@RequestParam boolean asc,
            @RequestParam boolean desc,
            @RequestParam Integer page, @RequestParam Integer size){
        if(asc && !desc)
            return categoryService.getAllCategoryOrderASC(page,size);
        else if (desc && !asc) {
            return categoryService.getAllCategoryOrderDES(page,size);
        }
        else if (asc && desc)
        {
            throw new InvalidFieldCategoryException("Invalid !");
        }else {
            throw new InvalidFieldCategoryException("Invalid !");
        }

    }
    @GetMapping("/categories/GetById/{id}")
    @Operation(summary = "Get category by id")
    public ResponseEntity<?> getCategoryById(@PathVariable("id") Integer categoryId){
        Category category = categoryService.getCategoryById(categoryId);
        CategoryResponse<Category> response = CategoryResponse.<Category>builder()
                .payload(category)
                .date(new Timestamp(System.currentTimeMillis()))
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update Category")
    public ResponseEntity<CategoryResponse<?>> updateCategory(@PathVariable("id") Integer categoryId, @RequestBody CategoryRequest categoryRequest){
        Integer category = categoryService.updateCategory(categoryRequest, categoryId);
        CategoryResponse<Category> response = CategoryResponse.<Category>builder()
                .payload(categoryService.getCategoryById(categoryId))
                .date(new Timestamp(System.currentTimeMillis()))
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }


    @GetMapping("/categories/currentCategory")
    @Operation(summary = "Get all category for current user")
    public ResponseEntity<CategoryResponse<List<Category>>> getAllTaskCurrentUser(@RequestParam boolean asc,@RequestParam boolean des,@RequestParam Integer size, @RequestParam Integer page){
       if(asc && !des){
           CategoryResponse<List<Category>> response = CategoryResponse.<List<Category>>builder()
//                .payload(categoryService.getAllCategoryCurrentUser(getUserCurrentId(),size, page))
                   .payload(categoryService.getCategoryCurrentUserByIdAsc(getUserCurrentId(),page,size))
                   .date(new Timestamp(System.currentTimeMillis()))
                   .success(true)
                   .build();
           return ResponseEntity.ok(response);
       } else if (!asc && des) {
           CategoryResponse<List<Category>> response = CategoryResponse.<List<Category>>builder()
                .payload(categoryService.getAllCategoryCurrentUser(getUserCurrentId(),size, page))
//                   .payload(categoryService.getCategoryCurrentUserByIdAsc(getUserCurrentId(),page,size))
                   .date(new Timestamp(System.currentTimeMillis()))
                   .success(true)
                   .build();
           return ResponseEntity.ok(response);
       }
       else if (asc && des)
       {
           throw new InvalidFieldCategoryException("Invalid !");
       }else {
           throw new InvalidFieldCategoryException("Invalid !");
       }
    }

    @GetMapping("/categories/{id}/users")
    @Operation(summary = "Get category by id for current user")
    public ResponseEntity<CategoryResponse<Category>> getCategoryByIdForCurrentUser(@PathVariable("id") Integer categoryId){
        CategoryResponse<Category> response = CategoryResponse.<Category>builder()
                .payload(categoryService.getCategoryIdByCurrentUser(getUserCurrentId(),categoryId))
                .date(new Timestamp(System.currentTimeMillis()))
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }

}
