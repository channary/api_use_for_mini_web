package com.example.miniproject.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskResponse<T> {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;

    private Timestamp date;
    Boolean success;
}
