package com.example.miniproject.service;
import com.example.miniproject.model.entity.Category;
import com.example.miniproject.model.entity.Task;
import com.example.miniproject.model.request.CategoryRequest;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
public interface CategoryService {



    List<Category> getAllCategory(Integer page, Integer size);
    List<Category> getAllCategoryOrderASC(Integer page, Integer size);
    List<Category> getAllCategoryOrderDES(Integer page, Integer size);

    Integer addNewCategory(CategoryRequest categoryRequest, Integer userId);

    Boolean deleteCategory(Integer categoryId,Integer userId);
//    List<Category> getAllCategory();

    Category getCategoryById(Integer categoryId);

//    Integer updateCategory(Integer categoryId);
    Integer updateCategory(CategoryRequest categoryRequest, Integer categoryId );

    List<Category> getAllCategoryCurrentUser(Integer categoryId ,Integer page, Integer size);

    Category getCategoryIdByCurrentUser(Integer currentUserId ,Integer categoryId);
    List<Category> getCategoryCurrentUserByIdAsc(Integer currentUserId,Integer page,Integer size);
}
