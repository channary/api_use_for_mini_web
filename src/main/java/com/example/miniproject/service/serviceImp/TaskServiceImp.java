package com.example.miniproject.service.serviceImp;



import com.example.miniproject.exception.EmptyFieldTaskException;
import com.example.miniproject.exception.InvalidFieldTaskException;
import com.example.miniproject.model.entity.Category;
import com.example.miniproject.model.entity.Task;
import com.example.miniproject.model.request.TaskRequest;
import com.example.miniproject.repository.CategoryRepository;
import com.example.miniproject.repository.TaskRepository;
import com.example.miniproject.service.TaskService;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class TaskServiceImp implements TaskService {
    private final TaskRepository taskRepository;
    private final CategoryRepository categoryRepository;

    public TaskServiceImp(TaskRepository taskRepository, CategoryRepository categoryRepository) {
        this.taskRepository = taskRepository;
        this.categoryRepository = categoryRepository;
    }


    @Override
    public List<Task> getAllTaskCurrentUser(Integer userId, Integer page, Integer size) {
        List<Task> allTask = taskRepository.findAllTaskCurrentUser(userId,page,size);
        if (allTask.isEmpty()){
            throw new EmptyFieldTaskException("There is no task for current user");
        }
        return allTask;
    }

    @Override
    public List<Task> getAllTaskCurrentUserOrderByDesc(Integer userId, Integer page, Integer size) {
        List<Task> allTaskDesc = taskRepository.findAllTaskCurrentUserDesc(userId,page,size);
        if (allTaskDesc.isEmpty()){
            throw new EmptyFieldTaskException("There is no task for current user");
        }
        return allTaskDesc;
    }


    @Override
    public List<Task> getAllTaskByOrderDes(Integer page, Integer size) {
        List<Task> allTask = taskRepository.getAllTaskOrderDesc(page, size);
        if (allTask.isEmpty()){
            throw new EmptyFieldTaskException("There are no record here !");
        }
        return taskRepository.getAllTaskOrderDesc(page,size);
    }




    @Override
    public Task getAllTaskByIdCurrentUser(Integer taskId, Integer userId) {
//        Task task = taskRepository.getTaskByIdCurrentUser(taskId,userId);
//        if (task == null){
//            throw new InvalidFieldTaskException("Task with id: " + taskId + " not belong to current user");
//        }
        return taskRepository.getTaskByIdCurrentUser(taskId,userId);
    }



    @Override
    public List<Task> getTaskByStatusCurrentUser(Integer userId,String status) {
        List<Task> task = taskRepository.getTaskByStatusCurrentUser(userId,status);
        if (!(status.equals("is_cancelled") ||
                status.equals("is_completed")||
                status.equals("is_in_progress")||
                status.equals("is_in_review"))
        ) {
            throw new InvalidFieldTaskException("This status is not correct : 'string' , please input" +
                    " one of (is_cancelled, is_completed, is_in_progress, is_in_review)");
        }
        if (task == null){
            throw new EmptyFieldTaskException("There is no task for current user");
        }
        return taskRepository.getTaskByStatusCurrentUser(userId,status);
    }



    @Override
    public Integer addNewTask(TaskRequest taskRequest, Integer userId) {
        Category categoryId =  categoryRepository.getCategoryById(taskRequest.getCategoryId());
        if (taskRequest.getTaskName().isBlank()){
            throw new InvalidFieldTaskException("taskName can't be blank");
        }
        else if (taskRequest.getDescription().isBlank()) {
            throw new InvalidFieldTaskException("description can't be blank");
        }
        else if (!(taskRequest.getStatus().equals("is_cancelled") ||
                taskRequest.getStatus().equals("is_completed")||
        taskRequest.getStatus().equals("is_in_progress")||
                taskRequest.getStatus().equals("is_in_review"))
        ) {
            throw new InvalidFieldTaskException("This status is not correct : 'string' , please input" +
                    " one of (is_cancelled, is_completed, is_in_progress, is_in_review)");
        }

        else if (categoryId == null){
            throw new EmptyFieldTaskException("categoryId " + taskRequest.getCategoryId() + " not belong to current user");
        }
        else if (taskRequest.getDate().equals(new Timestamp(System.currentTimeMillis()))){
            throw new InvalidFieldTaskException("date format is timestamp");
        }
        return taskRepository.insertTask(taskRequest,userId);
    }

    @Override
    public Boolean deleteTask(Integer taskId, Integer userId) {
        Boolean deletedTaskId = taskRepository.deleteTaskById(taskId,userId);
        if (!deletedTaskId){
            throw new InvalidFieldTaskException("Cannot delete this task because you are not the owner !!");
        }
        return true;
    }


    @Override
    public List<Task> getAllTask(Integer page, Integer size){
        if (taskRepository.getAllTask(page,size).isEmpty())
            throw new InvalidFieldTaskException("There are no record here !");
        else
        return taskRepository.getAllTask(page,size);
    }


    @Override
    public Task getTaskById(Integer taskId)  {
        if (taskRepository.getTaskById(taskId)!=null)
            return taskRepository.getTaskById(taskId);
        else
            throw new InvalidFieldTaskException("Task id " + taskId + " is not found !");
    }

    @Override
    public Integer updateTaskById(Integer taskId, TaskRequest taskRequest) {
        if (taskRepository.updateTask(taskId,taskRequest) != null){
            return taskRepository.updateTask(taskId, taskRequest);
        }
        else
            throw new InvalidFieldTaskException("Task id " + taskId + " is not found !");

    }

}
