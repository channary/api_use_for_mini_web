package com.example.miniproject.service.serviceImp;
import com.example.miniproject.exception.EmptyFieldCategoryException;
import com.example.miniproject.exception.InvalidFieldCategoryException;
import com.example.miniproject.exception.InvalidFieldTaskException;
import com.example.miniproject.model.entity.Category;
import com.example.miniproject.model.entity.Task;
import com.example.miniproject.model.entity.UserInfo;
import com.example.miniproject.model.request.CategoryRequest;
import com.example.miniproject.repository.CategoryRepository;
import com.example.miniproject.service.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CategoryServiceImp implements CategoryService {

    public Integer getUserCurrentId(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = (UserInfo) authentication.getPrincipal();
        int userId = userInfo.getId();
        return userId;
    }
    private final CategoryRepository categoryRepository;

    @Override
    public List<Category> getAllCategory(Integer page, Integer size) {
        List<Category> allCategory = categoryRepository.getAllCategory(page,size);
        if(allCategory.isEmpty())
            throw new InvalidFieldCategoryException("Invalid !!");
        else
        return categoryRepository.getAllCategory(page, size);
    }

    @Override
    public List<Category> getAllCategoryOrderASC(Integer page, Integer size) {
          List<Category> allCategory = categoryRepository.getAllCategoryOrderAsc(page,size);
        if(allCategory.isEmpty())
            throw new InvalidFieldCategoryException("Invalid !!");
        else
            return categoryRepository.getAllCategory(page, size);
    }

    @Override
    public List<Category> getAllCategoryOrderDES(Integer page, Integer size) {
          List<Category> allCategory = categoryRepository.getAllCategoryOrderAsc(page,size);
        if(allCategory.isEmpty())
            throw new InvalidFieldCategoryException("Invalid !!");
        else
            return categoryRepository.getAllCategoryOrderDes(page, size);
    }

    @Override
    public Integer addNewCategory(CategoryRequest categoryRequest,Integer userId) {
        if (categoryRequest.getCategoryName().isBlank()){
            throw new InvalidFieldCategoryException("CategoryName can't be blank");
        }
        return categoryRepository.insertCategory(categoryRequest,userId);
    }



    @Override
    public Boolean deleteCategory(Integer categoryId,Integer userId) {
        Boolean deletedCategoryId = categoryRepository.deleteCategoryById(categoryId,userId);
        if (!deletedCategoryId){
            throw new InvalidFieldCategoryException("Category with id  " + categoryId +  "  not exist");
        }
        return true;
    }

    @Override
    public Category getCategoryById(Integer categoryId) {
        if(categoryRepository.getCategoryById(categoryId)!=null)
        {
            return categoryRepository.getCategoryById(categoryId);
        } else
            throw new InvalidFieldCategoryException("Category with id  " + categoryId +  "  not exist");
    }

    @Override
    public Integer updateCategory(CategoryRequest categoryRequest, Integer categoryId) {
        if (categoryRepository.updateCategory(categoryRequest, categoryId,getUserCurrentId())!=null)
        {
            return categoryRepository.updateCategory(categoryRequest,categoryId,getUserCurrentId());
        } else if (categoryRequest.getCategoryName().isBlank()) {
            throw new InvalidFieldCategoryException("Category with id " + categoryId +" Can't be null");

        } else
            throw new InvalidFieldCategoryException("Category with id " + categoryId +" Invalid");

    }

    @Override
    public List<Category> getAllCategoryCurrentUser(Integer categoryId, Integer page, Integer size) {
//        List<Category> findAllCategory = categoryRepository.getCategoryAllCurrentUser(categoryId,page,size);

//        if (findAllCategory.isEmpty()){
//            throw new EmptyFieldCategoryException("There is no task for current user");
//        }
        return categoryRepository.getCategoryAllCurrentUser(categoryId,size,page);
    }

    @Override
    public Category getCategoryIdByCurrentUser(Integer currentUserId, Integer categoryId) {
        if(categoryRepository.getCategoryIdByCurrentUser(currentUserId,categoryId)==null)
            throw new InvalidFieldCategoryException("No Record in list ");
        else
        return categoryRepository.getCategoryIdByCurrentUser(currentUserId,categoryId);
    }

    @Override
    public List<Category> getCategoryCurrentUserByIdAsc(Integer currentUserId, Integer page, Integer size) {
        return categoryRepository.getAllCurrentCategoryOrderAsc(currentUserId,page,size);
    }
}
