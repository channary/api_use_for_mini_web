package com.example.miniproject.service;

import com.example.miniproject.model.entity.Task;
import com.example.miniproject.model.request.TaskRequest;

import java.util.List;

public interface TaskService {
    List<Task> getAllTaskCurrentUser(Integer userId,Integer page,Integer size);

    List<Task> getAllTaskCurrentUserOrderByDesc(Integer userId,Integer page,Integer size);
    List<Task> getAllTaskByOrderDes(Integer page, Integer size);

    Task getAllTaskByIdCurrentUser(Integer taskId, Integer userId);

    List<Task> getTaskByStatusCurrentUser(Integer userId,String status);

    Integer addNewTask(TaskRequest taskRequest, Integer userId);

    Boolean deleteTask(Integer taskId,Integer userId);

    List<Task> getAllTask(Integer page, Integer size);


    Task getTaskById(Integer taskId);

    Integer updateTaskById(Integer taskId, TaskRequest taskRequest);

}
